# News
Dies Ist die Startseite der SaS-Seite. Hier werden aktuelle Informationen vom Organisationskommittee eingetragen.
Aufgerufen wird die Seite unter <code>https://sas.stifts.net/</code> bzw. <code>https://sas.stifts.net/news/</code>

zurückgegeben werden Json-Antworten nach dem [InfoResult-Model](http://docs.codinjowa.de/de/latest/api/InfoResults/).
## Data Models
### NewsIndex
    public class NewsIndex
    {
        public int idNews { get; set; }
        public string title { get; set; }
        public string text { get; set; }
        public string image { get; set; //Link zu einem Dazu gehörigen Bild
        public string link { get; set; }  //Link für weiter Informationen
        public DateTime? timestamp { get; set; } //Zeitpunkt ab dem die News gezeigt werden soll
    }
### NewsUpdate <code>extends NewsIndex</code>
    public class NewsUpdate : NewsIndex
    {
        public int? idRole { get; set; }
    }
<br />
##StatusCodes
Dies Sind die Möglichen Exceptions für den NewsController

    public class NewsInfo
        {
            public const string NotFound = "Die angefragte News konnte nicht gefunden werden."; -> 404
            public const string BadRequest = "Bitte geben Sie alle erforderlichen Felder an"; -> 400
            public const string Created = "News erfolgreich erstellt."; -> 200
            public const string Updated = "News erfolgreich aktualisiert."; -> 200
            public const string Deleted = "News erfolgreich gelöscht."; -> 200
            public const string InternalServerError = Exception.Message; -> 500
        }
<br />
## Api Requests
### Index <code>/news</code>
Es wird eine Json-Liste aller Verfügnbaren News zurückgegeben. Welche News Verfügbar sind hängt von der Rolle/den Berechtigungen des Benutzers, sowie dem Aktuellen Datum ab.
#### GET
##### Result <code>type: InfoResult; Model type: NewsIndex[]</code>
Erfolgreich:

    {
        "StatusCode": 200,
        "ShortInfo": "Status200OK",
        "StackTrace": null,
        "Model": [
            {
                "idNews": 14,
                "title": "lol",
                "text": "mimimi",
                "image": null,
                "link": null,
                "timestamp": "2019-03-28T15:02:16.455"
            },
            {
                "idNews": 15,
                "title": "lol",
                "text": "mimimi",
                "image": null,
                "link": null,
                "timestamp": "2019-03-28T15:02:16.455"
            },
            {
                "idNews": 16,
                "title": "lol",
                "text": "mimimi",
                "image": null,
                "link": null,
                "timestamp": "2019-03-29T15:02:16.455"
            },
            {
                "idNews": 21,
                "title": "hi",
                "text": "birnenTraum",
                "image": null,
                "link": null,
                "timestamp": "2019-03-29T15:02:16.455"
            }
        ]
    }

Exceptions Bsp:

    {
        "StatusCode": 500,
        "ShortInfo": "Exception of type 'System.Exception' was thrown.",
        "StackTrace": "   at sasBackend.Controllers.NewsController.Index() in sasBackend\\Controllers\\NewsController.cs:line 27",
        "Model": null
    }

Mögliche Exceptions:

    public const string InternalServerError = Exception.Message; -> 500
<br />
### Create <code>/Create</code>
Es wird ein Json Objekt mit den relevanten Informationen an den Server Gesendet, um neue News hinzuzufügen. Benutzergruppen: <code>Authorization -> Benutzergruppen</code>
#### POST
##### Data <code>type: NewsUpdate</code>
    {
        "idRole": 5, //Abstimmungsgruppe
        "idNews": 13,
        "title": "Titel 1",
        "text": "Text 1",
        "image": "image_url",
        "link": "additional_link",
        "timestamp": "2019-03-28T15:02:16.455" //Zeitpunkt ab dem die News Sichtbar ist
    }
#### Result <code>type: Inforesult; Model type: NewsUpdate</code>
Erfolgreich:

    {
        "StatusCode": 200,
        "ShortInfo": "News erfolgreich erstellt.",
        "StackTrace": null,
        "Model": {
            "idRole": null,
            "idNews": 23,
            "title": "hi",
            "text": "lul",
            "image": null,
            "link": null,
            "timestamp": null
        }
    }

Exception Bsp:

    {
        "StatusCode": 400,
        "ShortInfo": "Bitte geben Sie alle erforderlichen Felder an",
        "StackTrace": "Bitte geben Sie alle erforderlichen Felder an",
        "Model": {
            "idRole": null,
            "idNews": 0,
            "title": "hi",
            "text": null,
            "image": null,
            "link": null,
            "timestamp": null
        }
    }

Mögliche Exceptions:

    public const string BadRequest = "Bitte geben Sie alle erforderlichen Felder an"; -> 400
    public const string InternalServerError = Exception.Message; -> 500
<br />
### Update <code>/Update/{int? id}</code>
Wird verwendet zum Bearbeiten einer News. <code>id</code> ist hierbei ein Integer und representiert die ID der News.
#### GET
##### Result <code>type: InfoResult; Model type: NewsUpdate</code>
Erfolgreich:

    {
        "StatusCode": 200,
        "ShortInfo": "Status200OK",
        "StackTrace": null,
        "Model": {
            "idRole": 2,
            "idNews": 21,
            "title": "hi",
            "text": "birnenTraum",
            "image": null,
            "link": null,
            "timestamp": "2019-03-29T15:02:16.455"
        }
    }

Mögliche Exceptions:

  public const string NotFound = "Die angefragte News konnte nicht gefunden werden."; -> 404

#### POST
##### Data <code>type: NewsUpdate</code>
    {
        "idRole": 2,
        "idNews": 21,
        "title": "Apfeltraum",
        "text": "birnenTraum",
        "image": null,
        "link": null,
        "timestamp": "2019-03-29T15:02:16.455"
    }
##### Result <code>type: InfoResult; Model type: NewsUpdate</code>
Erfolgreich:

    {
        "StatusCode": 200,
        "ShortInfo": "News erfolgreich aktualisiert.",
        "StackTrace": null,
        "Model": {
            "idRole": 2,
            "idNews": 21,
            "title": "Apfeltraum",
            "text": "birnenTraum",
            "image": null,
            "link": null,
            "timestamp": "2019-03-29T15:02:16.455"
        }
    }

Exception Bsp:

    {
        "StatusCode": 500,
        "ShortInfo": "An error occurred while updating the entries. See the inner exception for details.",
        "StackTrace": "   at Microsoft.EntityFrameworkCore.Update.ReaderModificationCommandBatch.ExecuteAsync(IRelationalConnection connection, CancellationToken cancellationToken)\r\n [...]",
        "Model": null
    }

Mögliche Exceptions:

    public const string NotFound = "Die angefragte News konnte nicht gefunden werden."; -> 404
    public const string BadRequest = "Bitte geben Sie alle erforderlichen Felder an"; -> 400
    public const string InternalServerError = Exception.Message; -> 500
<br />
### Delete <code>/Delete/{int? id}</code>
Wird verwendet um eine News zu löschen.
#### GET
##### Result <code>type: InfoResult</code>
Erfolgreich:

    {
        "StatusCode": 200,
        "ShortInfo": "News erfolgreich gel?scht.",
        "StackTrace": null,
        "Model": null
    }

Exception Bsp:

    {
        "StatusCode": 404,
        "ShortInfo": "Die angefragte News konnte nicht gefunden werden.",
        "StackTrace": "Die angefragte News konnte nicht gefunden werden.",
        "Model": null
    }

Mögliche Exceptions:

    public const string NotFound = "Die angefragte News konnte nicht gefunden werden."; -> 404
    public const string InternalServerError = Exception.Message; -> 500
