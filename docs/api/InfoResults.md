# InfoResults
Sogenannte InfoResults werden als Antworten auf Anfragen zurück gesendet.

Alles, was nicht StatusCode = 200 hat ist als Exception zu betrachten.
## Data Model
### ResultModel
    public class ResultModel
    {
        [Required]
        public int StatusCode { get; set;  //HttpStatus Code zum Identifizieren von Infosund Exceptions
        [Required]
        public string ShortInfo { get; set; } //Kurze Beschreibung der exception/Info
        public string StackTrace { get; set; }  //StackTrace der exception (falls vorhanden)
        public object Model { get; set; } //Schickt das relevante Object zurück (Beim aktualisieren das aktualisierte Objekt)
    }
