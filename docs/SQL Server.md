# SaS SQL-Server aufsetzen
In unserem Beispiel wird der <b>Microsoft SQL Server 2017</b> verwendet, prinzipiell ist jedoch auch jede neuere Version möglich.
Nachdem der Server installiert und nach belieben durchkonfiguriert wurde (siehe [Microsoft Docs](https://docs.microsoft.com/de-de/sql/database-engine/install-windows/install-sql-server?view=sql-server-2017)),
müssen die im Root-Directory des sasBackend Repos abgelegten SQL-Dateien ausgeführt werden.
## Zu beachten ist folgende Reihenfolge:
<li>1. zentraldb-schema.sql</li>
<li>2. sas-schema.sql</li>
<li>3. sas-sampledata.sql</li>
<li>4. sas-schemaconstraints.sql</li>
