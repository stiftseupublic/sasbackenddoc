# How to build an Exe
Um eine Exe Datei zu generieren:
<li>1. Projekt in Visual Studio öffnen</li>
<li>2. Unter <b>Extras -> NuGet-Paket-Manager</b> auf <b>Paket-Manager-Kosnole</b> klicken</li>
<li>3. in der geöffneten PowerShell-Konsole folgenden Befehl eingeben:</li>
    <code>dotnet publish -c release -r win10-x64</code>

# Download the latest builded version
[Download](https://gitlab.com/stifts-eu/sas/sasbackend.git)
